package com.android_example.shared.data.usecases

import com.android_example.shared.CommonFlow
import com.android_example.shared.asCommonFlowWithDispatcherIo
import com.android_example.shared.data.network.api.NetworkApi
import com.android_example.shared.data.cache.storage.TokenStorage
import com.android_example.shared.domain.entities.api.request.ConfirmCodeRequestBody
import com.android_example.shared.domain.entities.api.request.PhoneVerificationRequestBody
import com.android_example.shared.domain.entities.api.response.CheckPhoneAuthResponse
import com.android_example.shared.domain.entities.api.response.Token
import com.android_example.shared.domain.usecases.IAuthUseCase
import kotlinx.coroutines.flow.flow

class AuthUseCaseImpl(
    private val api: NetworkApi,
    private val tokenStorage: TokenStorage
) : IAuthUseCase {

    override fun checkPhone(phone: String): CommonFlow<String> {
        return flow {
            val response = api.checkPhone(phone = phone)
            emit(response.phone)
        }.asCommonFlowWithDispatcherIo()
    }
}
