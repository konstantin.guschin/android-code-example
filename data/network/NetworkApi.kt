package com.android_example.shared.data.network.api

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.features.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.json.Json

class LoggerDelegate(val delegate: (String) -> Unit) : Logger {
    override fun log(message: String) {
        delegate(message)
    }
}

class NetworkApi(
    private val engine: HttpClientEngine,
    private val config: ApiConfig,
    private val storage: AppStorage,
    private val tokenStorage: TokenStorage,
    private val timeZone: String,
    private val isLogEnabled: Boolean,
    private val logFunction: ((String) -> Unit)? = null,
) {

    private val noDefaultsJson by lazy {
        Json {
            encodeDefaults = false
            ignoreUnknownKeys = true
            isLenient = true
        }
    }

    private val client
        get() = HttpClient(engine) {
            if (isLogEnabled) {
                Logging {
                    logger = logFunction?.let(::LoggerDelegate) ?: Logger.SIMPLE
                    level = LogLevel.ALL
                }
            }

            Json {
                serializer = KotlinxSerializer(noDefaultsJson)
            }

            Authenticator {
                refreshUrl = "/api/v1/user/refreshToken"
                storage = tokenStorage
            }

            defaultRequest {
                host = config.baseUrl
                url {
                    protocol = URLProtocol.HTTPS
                }

                if (!attributes.contains(MultiPartAttribute)) {
                    contentType(ContentType.Application.Json)
                }
                header("platform", platform)
                header("version", "1")
                header("language", storage.selectedLanguage)
                header("gmt", timeZone)
                if (attributes.contains(RefreshAttribute)) {
                    header("token", tokenStorage.fetchRefreshToken())
                } else {
                    header("token", tokenStorage.fetchAccessToken())
                }
            }

            addDefaultResponseValidation()
            addResponseExceptionHandler()
        }

    suspend fun checkPhone(phone: String): CheckPhoneResponse {
        return client.post("/api/v1/user/checkPhone") {
            body = CheckPhoneRequestBody(phone)
            attributes.put(PhoneConfirmAttribute, Unit)
        }
    }
}
