package com.android_example.presentation.screens.auth.phone.contract

import com.android_example.utils.lifecycle.Intention

sealed class PhoneIntentions : Intention {

    data class AfterPhoneChange(val phone: String) : PhoneIntentions()
    object OnGetSmsCodeClick : PhoneIntentions()
}
