package com.android_example.presentation.screens.auth.phone.contract

import com.android_example.utils.lifecycle.UiState

data class PhoneUiState(
    val phone: String,
    val isGetSmsButtonEnable: Boolean,
    val isLoading: Boolean,
) : UiState {
    companion object {
        val INITIAL = PhoneUiState(
            phone = "",
            isGetSmsButtonEnable = false,
            isLoading = false,
        )
    }
}
