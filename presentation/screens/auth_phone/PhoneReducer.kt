package com.android_example.presentation.screens.auth.phone

import androidx.lifecycle.viewModelScope
import com.android_example.NavGraphDirections
import com.android_example.presentation.base.effects.Navigate
import com.android_example.presentation.base.effects.ShowMessageEffect
import com.android_example.presentation.base.viewmodel.Reducer
import com.android_example.presentation.model.UserProfile
import com.android_example.presentation.screens.auth.phone.contract.PhoneIntentions
import com.android_example.presentation.screens.auth.phone.contract.PhoneUiState
import com.android_example.shared.Validators
import com.android_example.shared.data.repository.LaunchRepository
import com.android_example.shared.data.cache.storage.DeAuthorizationType
import com.android_example.shared.domain.usecases.IAuthUseCase
import com.android_example.utils.converters.ErrorConverter
import com.android_example.utils.converters.toRequestPhoneFormat
import com.android_example.utils.mask.phone.PhoneLengthCalculator
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

@HiltViewModel
class PhoneReducer @Inject constructor(
    private val authUseCase: IAuthUseCase,
    private val error: ErrorConverter,
    private val phoneLengthCalculator: PhoneLengthCalculator,
    private val repository: LaunchRepository,
    private val validators: Validators,
) : Reducer<PhoneUiState, PhoneIntentions>() {

    init {
        repository.deAuthorization
            .filter { it == DeAuthorizationType.UNKNOWN_PHONE }
            .onEach {
                val userProfile = UserProfile().copy(phone = state.phone)
                val event = Navigate(NavGraphDirections.actionGlobalToRegistrationGraph(userProfile))
                offerEffect(effect = event)
            }
            .launchIn(viewModelScope)

        validators.phoneValidator.observeFieldState()
            .filter { it.isValid }
            .onEach { fieldState ->
                val phoneMaxLength = phoneLengthCalculator.calculateMaxPhoneLength(phone = fieldState.value)
                if (fieldState.value.length <= phoneMaxLength) {
                    state = state.copy(
                        phone = fieldState.value,
                        isGetSmsButtonEnable = fieldState.value.length == phoneMaxLength,
                    )
                    if (fieldState.value.length == phoneMaxLength) {
                        offerComposeEffect(composeEffect = HideKeyboardEffect)
                    }
                }
            }
            .launchIn(viewModelScope)
    }

    override fun createInitialState(): PhoneUiState = PhoneUiState.INITIAL

    override fun onIntention(intention: PhoneIntentions) {
        when (intention) {
            is PhoneIntentions.AfterPhoneChange -> afterPhoneChanged(phone = intention.phone)
            PhoneIntentions.OnGetSmsCodeClick -> onGetSmsCodeClicked()
        }
    }

    private fun onGetSmsCodeClicked() {
        val phone = state.phone.toRequestPhoneFormat()
        authUseCase.checkPhone(phone = phone)
            .onStart { state = state.copy(isLoading = true) }
            .onCompletion { state = state.copy(isLoading = false) }
            .onEach {
                val directions = NavGraphDirections.actionGlobalToCallConfirmFragment(phone = phone, checkPhone = it)
                val effect = Navigate(directions = directions)
                offerEffect(effect = effect)
            }
            .catch { cause: Throwable ->
                val error = error.convert(throwable = cause)
                val effect = ShowMessageEffect(message = error.message)
                offerComposeEffect(composeEffect = effect)
            }
            .launchIn(viewModelScope)
    }

    private fun afterPhoneChanged(phone: String) {
        validators.phoneValidator.setText(text = changePrefix(text = phone))
    }

    private fun changePrefix(text: String): String {
        return when {
            text.startsWith(prefix = "8") -> text.replaceFirstChar { "7" }
            else -> text
        }
    }
}
