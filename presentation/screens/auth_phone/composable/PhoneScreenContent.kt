package com.android_example.presentation.screens.auth.phone.composable

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.android_example.R
import com.android_example.presentation.composable.theme.app.AppTheme
import com.android_example.presentation.screens.auth.phone.contract.PhoneIntentions
import com.android_example.presentation.screens.auth.phone.contract.PhoneUiState
import com.android_example.utils.mask.phone.PhoneMaskTransformation

private val phoneUiState = PhoneUiState(
    phone = "",
    isGetSmsButtonEnable = false,
    isLoading = false,
)

@Suppress("UnusedPrivateMember")
@Composable
@Preview(showSystemUi = true, showBackground = true)
private fun PhoneScreenContentPreview() {
    AppTheme {
        Scaffold {
            PhoneScreenContent(
                phoneUiState = phoneUiState,
                offerIntention = {},
            )
        }
    }
}

@Composable
fun PhoneScreenContent(
    phoneUiState: PhoneUiState,
    offerIntention: (PhoneIntentions) -> Unit,
) {

    val keyboardController = LocalSoftwareKeyboardController.current
    val scrollState = rememberScrollState()

    Column(
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState),
    ) {
        Text(
            text = stringResource(R.string.phone_title_description),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = AppTheme.dimens.dp60)
                .padding(top = AppTheme.dimens.dp100),
            style = MaterialTheme.typography.h5,
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(AppTheme.dimens.dp10),
        )
        Text(
            text = stringResource(R.string.phone_under_title_description),
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = AppTheme.dimens.dp60),
            style = MaterialTheme.typography.body1,
            textAlign = TextAlign.Start,
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(AppTheme.dimens.dp90),
        )
        TextField(
            value = phoneUiState.phone,
            onValueChange = { offerIntention(PhoneIntentions.AfterPhoneChange(phone = it)) },
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = AppTheme.dimens.dp60),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Number, imeAction = ImeAction.Done,
            ),
            keyboardActions = KeyboardActions(onDone = { keyboardController?.hide() }),
            visualTransformation = PhoneMaskTransformation(),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                cursorColor = Color.LightGray,
            ),
        )
        Spacer(
            modifier = Modifier
                .fillMaxWidth()
                .height(AppTheme.dimens.dp15),
        )
        Button(
            onClick = {
                offerIntention(PhoneIntentions.OnGetSmsCodeClick)
                keyboardController?.hide()
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = AppTheme.dimens.dp60),
            enabled = phoneUiState.isGetSmsButtonEnable,
        ) {
            Text(text = stringResource(id = R.string.phone_confirm_button))
        }
    }
}
