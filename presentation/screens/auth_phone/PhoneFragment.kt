package com.android_example.presentation.screens.auth.phone

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHost
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.fragment.app.viewModels
import com.android_example.presentation.base.effects.ShowMessageEffect
import com.android_example.presentation.base.fragment.BaseFragment
import com.android_example.presentation.composable.error.ErrorSnackbar
import com.android_example.presentation.composable.theme.AppTheme
import com.android_example.presentation.screens.auth.phone.composable.PhoneScreenContent
import com.android_example.utils.lifecycle.CollectEffects
import com.android_example.utils.lifecycle.collectAsStateWithLifecycle
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PhoneFragment : BaseFragment<PhoneReducer>() {

    override val viewModel by viewModels<PhoneReducer>()

    @Composable
    override fun ComposeContent() {

        val phoneUiState by viewModel.uiState.collectAsStateWithLifecycle()
        val scaffoldState = rememberScaffoldState()
        val keyboardController = LocalSoftwareKeyboardController.current

        CollectEffects(viewModel) { effect ->
            when (effect) {
                is ShowMessageEffect -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = effect.message,
                        duration = SnackbarDuration.Long,
                    )
                }

                is HideKeyboardEffect -> {
                    keyboardController?.hide()
                }
            }
        }
        Scaffold {
            Box(modifier = Modifier.fillMaxSize()) {
                if (phoneUiState.isLoading) {
                    CircularProgressIndicator(
                        modifier = Modifier.align(alignment = Alignment.Center),
                    )
                } else {
                    PhoneScreenContent(
                        phoneUiState = phoneUiState,
                        offerIntention = viewModel::offerIntention,
                    )
                }

                SnackbarHost(
                    modifier = Modifier.padding(AppTheme.dimens.dp16),
                    hostState = scaffoldState.snackbarHostState,
                    snackbar = { data ->
                        ErrorSnackbar(message = data.message)
                    },
                )
            }
        }
    }
}
