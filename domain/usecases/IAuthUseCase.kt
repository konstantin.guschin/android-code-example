package com.android_example.shared.domain.usecases

import com.android_example.shared.CommonFlow
import com.android_example.shared.domain.entities.api.response.CheckPhoneAuthResponse

interface IAuthUseCase {
    fun checkPhone(phone: String): CommonFlow<String>
}
